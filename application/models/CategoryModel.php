<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryModel extends CI_Model
{
    public function listAll()
    {
        $this->db->select("*")
            ->from("category")
            ->where("active", "1");

        $result = $this->db->get()->result();

        return $result;
    }

}

