<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NewsModel extends CI_Model
{
    public function listAll()
    {
        $this->db->select("news.*, category.name as category_name, user_info.name as user_name")
            ->from("news")
            ->join("category", "news.category_id = category.id")
            ->join("user_info", "news.user_info_id = user_info.id")
            ->where("news.active", "1")
            ->order_by("news.create_date", "desc")
            ->limit(50, 0);

        $result = $this->db->get()->result();

        return $result;
    }

    public function listOne($id)
    {
        $this->db->select("news.*, category.name as category_name, user_info.name as user_name, user_info.bio as user_bio, user_info.image_url as user_image ")
            ->from("news")
            ->join("category", "news.category_id = category.id")
            ->join("user_info", "news.user_info_id = user_info.id")
            ->where("news.active", "1")
            ->where('news.id', $id);

        $result = $this->db->get()->result();
        return $result;
    }

    public function listAllToSingleNews($id)
    {
        $this->db->select("news.*, category.name as category_name, user_info.name as user_name")
            ->from("news")
            ->join("category", "news.category_id = category.id")
            ->join("user_info", "news.user_info_id = user_info.id")
            ->where("news.id !=", $id)
            ->where("news.active", "1")
            ->order_by("news.create_date", "desc")
            ->limit(50, 0);

        $result = $this->db->get()->result();
        return $result;
    }

    public function listOneCategory($id)
    {
        $this->db->select("news.*, category.name as category_name, user_info.name as user_name")
            ->from("news")
            ->join("category", "news.category_id = category.id")
            ->join("user_info", "news.user_info_id = user_info.id")
            ->where("news.active", "1")
            ->where('news.category_id', $id)
            ->order_by("news.create_date", "desc")
            ->limit(50, 0);

        $result = $this->db->get()->result();
        return $result;
    }

    public function listAllSearch($search)
    {
        $this->db->select("news.*, category.name as category_name, user_info.name as user_name")
            ->from("news")
            ->join("category", "news.category_id = category.id")
            ->join("user_info", "news.user_info_id = user_info.id")
            ->where("news.active", "1")
            ->like('news.title', $search)
            ->order_by("news.create_date", "desc")
            ->limit(50, 0);

        $result = $this->db->get()->result();

        return $result;
    }
}

