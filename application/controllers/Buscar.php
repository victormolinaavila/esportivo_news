<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buscar extends CI_Controller
{
    public function index()
    {
        $this->load->helper('url');
        $arraySearch = $this->uri->uri_to_assoc(1);
        $search = $arraySearch['buscar'];
        $search = str_replace("%20", " ", $search);
        $search = str_replace("-", " ", $search);

        $this->load->model("NewsModel", "news");
        $resultAll = $this->news->listAllSearch($search);

        $this->load->model("CategoryModel", "category");
        $resultAllCategories = $this->category->listAll();

        $data = array("categories" => $resultAllCategories, "news" => $resultAll);

        $this->load->view('BuscarView', $data);


    }
}
