<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{
    public function index()
        {
            $this->load->helper('url');
            $this->load->model("NewsModel", "news");
            $result = $this->news->listAll();

            $this->load->model("CategoryModel", "category");
            $resultAllCategories = $this->category->listAll();

            $data = array("categories" => $resultAllCategories, "news" => $result);

            $this->load->view('WelcomeView', $data);
        }
}
