<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{
    public function index()
        {
            $this->load->helper('url');
            $this->load->model("NewsModel", "news");
            $result = $this->news->listAll();

            $data = array("news" => $result);

            $this->load->view('SitemapView', $data);
        }
}
