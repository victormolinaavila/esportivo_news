<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticia extends CI_Controller
{
    public function index()
        {
            $this->load->helper('url');
            $news_id = $this->uri->segment(2,0);

            if ($news_id==0){
                redirect('/welcome/', 'welcome');
            } else {
                $this->load->model("NewsModel", "news");
                $resultOne = $this->news->listOne($news_id);
                if ($resultOne==null){
                    redirect('/welcome/', 'welcome');
                } else {
                    $resultAll = $this->news->listAllToSingleNews($news_id);

                    $this->load->model("CategoryModel", "category");
                    $resultAllCategories = $this->category->listAll();

                    $data = array("categories" => $resultAllCategories, "one_news" => $resultOne, "news" => $resultAll);

                    $this->load->view('NewsView', $data);
                }
            }
        }
}
