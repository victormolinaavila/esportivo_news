<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller
{

    public function index()
        {
            $this->load->helper('url');
            $category_id = $this->uri->segment(2,0);

            if ($category_id==0){
                redirect('/welcome/', 'welcome');
            } else {
                $this->load->model("NewsModel", "news");
                $resultOne = $this->news->listOneCategory($category_id);

                if ($resultOne==null){
                    redirect('/welcome/', 'welcome');
                } else {
                    $this->load->model("CategoryModel", "category");
                    $resultAllCategories = $this->category->listAll();

                    $data = array("categories" => $resultAllCategories,"one_category" => $resultOne);

                    $this->load->view('CategoryView', $data);
                }

            }
        }
}
