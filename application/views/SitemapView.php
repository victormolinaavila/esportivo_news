<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9">
    <?php foreach ($news as $n) { ?>
    <url>
        <loc><?php echo base_url() . 'noticia/' . $n->id; ?></loc>
        <news:news>
            <news:publication>
                <news:name><?php echo $n->title; ?></news:name>
                <news:language>pt</news:language>
            </news:publication>
            <news:publication_date><?php echo date('Y-m-d', strtotime($n->create_date)); ?></news:publication_date>
            <news:title><?php echo $n->title; ?></news:title>
        </news:news>
    </url>
    <?php } ?>
</urlset>

