<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
    <!-- Google -->
    <meta name="google-site-verification" content="R0lFFbrXcO3FXpSVOj8eR7CDbJFgfo7WJxTo4ekdSo0" />
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-2940248395445787",
            enable_page_level_ads: true
        });
    </script>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="https://portalnewsimage.s3.sa-east-1.amazonaws.com/logomini.png" async>
    <!-- Author Meta -->
    <meta name="author" content="colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Esportivo News</title>
    <!--CSS============================================= -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/linearicons.css" async>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css" async>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css" async>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css" async>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-132808119-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-132808119-1');
    </script>
</head>
<body>
<header>
    <div class="container-fluid main-menu" id="main-menu">
        <div class="row align-items-center justify-content-between">
            <nav id="nav-menu-container">
                <ul class="nav-menu mt-10">
                    <li>
                        <div class="col-lg-12 col-md-12 col-sm-12 no-padding logo">
                            <a href="<?php echo base_url(); ?>"> <a href="<?php echo base_url(); ?>"><img src="https://portalnewsimage.s3.sa-east-1.amazonaws.com/logo.png"></a></a>
                        </div>
                    </li>
                    <?php foreach ($categories as $c) { ?>
                        <li><a href="<?php echo base_url() . 'categoria/' . $c->id; ?>"
                               class="category-menu"><?php echo $c->name; ?></a></li>
                    <?php } ?>
                </ul>
            </nav><!-- #nav-menu-container -->
            <div class="toggle-login Search"><span class="lnr lnr-magnifier"></span></div>
            <div class="login">
                <div class="triangle"></div><!-- .triangle -->
                <form id="search_form">
                    <input class="search-input" type="text" placeholder="Ex: Futebol" id="search_name"/>
                    <input type="submit" value="Buscar" id="search_button"/>
                </form>
            </div><!--  .login -->
        </div>
    </div>
</header>

<div class="site-main-container">
    <!-- Start top-post Area -->
    <section class="top-post-area pt-10" id="top-post-area">
        <div class="container no-padding">
            <div class="row small-gutters">
                <div class="col-lg-8 top-post-left">
                    <?php foreach (array_slice($one_category, 0, 1) as $n) { ?>
                        <script>
                            <?php
                            $titulo_novo = preg_replace('/[ -]+/', '-', $n->category_name);
                            ?>
                            window.history.pushState("object or string", "Title", "/categoria/<?php echo $n->category_id; ?>/<?php echo $titulo_novo?>");
                        </script>
                        <div class="feature-image-thumb relative">
                            <div class="overlay overlay-bg"></div>
                            <a href="<?php echo base_url() . 'noticia/' . $n->id . '/' . str_replace(" ", "-", $n->title); ?>">
                                <img class="img-fluid max-min-height-overlay image-top-left" alt="<?php echo $n->title; ?>"
                                     srcset="https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/540x300-<?php echo $n->id; ?>.jpg 1x,https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/810x456-<?php echo $n->id; ?>.jpg 1.5x,https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/1080x608-<?php echo $n->id; ?>.jpg 2x"
                                     src="https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/810x456-<?php echo $n->id; ?>.jpg"></a>
                        </div>
                        <div class="top-post-details">
                            <a href="<?php echo base_url() . 'noticia/' . $n->id . '/' . str_replace(" ", "-", $n->title); ?>">
                                <h3><?php echo $n->title; ?></h3>
                            </a>
                            <a href="<?php echo base_url() . 'noticia/' . $n->id . '/' . str_replace(" ", "-", $n->title); ?>">
                                <ul class="meta">
                                    <li><span class="lnr lnr-calendar-full"></span><?php
                                        $start_date = $n->create_date;
                                        $date_diff = abs((strtotime("now") - 60 * 60 * 2) - strtotime($start_date));

                                        $years = floor($date_diff / (365 * 60 * 60 * 24));
                                        $months = floor(($date_diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                        $days = floor(($date_diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                                        $hours = floor(($date_diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
                                        $minutes = floor(($date_diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / 60);

                                        if ($years > 0) {
                                            condição ? codigoUm : codigoDois;
                                            $years == 1 ? printf("há %d ano", $years) : printf("há %d anos", $years);;
                                        } else {
                                            if ($months > 0) {
                                                $months == 1 ? printf("há %d mês", $months) : printf("há %d meses", $months);
                                            } else {
                                                if ($days > 0) {
                                                    $days == 1 ? printf("há %d dia", $days) : printf("há %d dias", $days);
                                                } else {
                                                    if ($hours > 0) {
                                                        $hours == 1 ? printf("há %d hora", $hours) : printf("há %d horas", $hours);
                                                    } else {
                                                        if ($minutes > 0) {
                                                            $minutes == 1 ? printf("há %d minuto", $minutes) : printf("há %d minutos", $minutes);
                                                        }
                                                    }
                                                }

                                            }
                                        } ?></li>
                                    <li><span class="lnr lnr-user"></span><?php echo $n->user_name; ?></li>
                                </ul>
                            </a>
                        </div>
                        <ul class="category-image-top-left">
                            <li>
                                <a href="<?php echo base_url() . 'categoria/' . $n->category_id; ?>"><?php echo $n->category_name; ?></a>
                            </li>
                        </ul>
                    <?php } ?>
                </div>
                <div class="col-lg-4 top-post-right">
                    <div class="single-top-post">
                        <?php foreach (array_slice($one_category, 1, 1) as $n) { ?>
                            <div class="feature-image-thumb relative">
                                <div class="overlay overlay-bg"></div>
                                <a href="<?php echo base_url() . 'noticia/' . $n->id . '/' . str_replace(" ", "-", $n->title); ?>">
                                    <img class="img-fluid max-min-height-overlay image-top-left" alt="<?php echo $n->title; ?>"
                                         srcset="https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/540x300-<?php echo $n->id; ?>.jpg 1x,https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/810x456-<?php echo $n->id; ?>.jpg 1.5x,https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/1080x608-<?php echo $n->id; ?>.jpg 2x"
                                         src="https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/810x456-<?php echo $n->id; ?>.jpg"></a>
                            </div>
                            <div class="top-post-details">
                                <a href="<?php echo base_url() . 'noticia/' . $n->id . '/' . str_replace(" ", "-", $n->title); ?>">
                                    <h4><?php echo $n->title; ?></h4>
                                </a>
                                <a href="<?php echo base_url() . 'noticia/' . $n->id . '/' . str_replace(" ", "-", $n->title); ?>">
                                    <ul class="meta">
                                        <li><span class="lnr lnr-calendar-full"></span><?php
                                            $start_date = $n->create_date;
                                            $date_diff = abs((strtotime("now") - 60 * 60 * 2) - strtotime($start_date));

                                            $years = floor($date_diff / (365 * 60 * 60 * 24));
                                            $months = floor(($date_diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                            $days = floor(($date_diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                                            $hours = floor(($date_diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
                                            $minutes = floor(($date_diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / 60);

                                            if ($years > 0) {
                                                condição ? codigoUm : codigoDois;
                                                $years == 1 ? printf("há %d ano", $years) : printf("há %d anos", $years);;
                                            } else {
                                                if ($months > 0) {
                                                    $months == 1 ? printf("há %d mês", $months) : printf("há %d meses", $months);
                                                } else {
                                                    if ($days > 0) {
                                                        $days == 1 ? printf("há %d dia", $days) : printf("há %d dias", $days);
                                                    } else {
                                                        if ($hours > 0) {
                                                            $hours == 1 ? printf("há %d hora", $hours) : printf("há %d horas", $hours);
                                                        } else {
                                                            if ($minutes > 0) {
                                                                $minutes == 1 ? printf("há %d minuto", $minutes) : printf("há %d minutos", $minutes);
                                                            }
                                                        }
                                                    }

                                                }
                                            } ?></li>
                                        <li><span class="lnr lnr-user"></span><?php echo $n->user_name; ?></li>
                                    </ul>
                                </a>
                            </div>
                            <ul class="category-image-top-right">
                                <li>
                                    <a href="<?php echo base_url() . 'categoria/' . $n->category_id; ?>"><?php echo $n->category_name; ?></a>
                                </li>
                            </ul>
                        <?php } ?>
                    </div>
                    <div class="single-top-post mt-10">
                        <?php foreach (array_slice($one_category, 2, 1) as $n) { ?>
                            <div class="feature-image-thumb relative">
                                <div class="overlay overlay-bg"></div>
                                <a href="<?php echo base_url() . 'noticia/' . $n->id . '/' . str_replace(" ", "-", $n->title); ?>"><img
                                            class="img-fluid" style="max-height: 216.15px"
                                            srcset="https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/540x300-<?php echo $n->id; ?>.jpg 1x,https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/810x456-<?php echo $n->id; ?>.jpg 1.5x,https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/1080x608-<?php echo $n->id; ?>.jpg 2x"
                                            src="https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/810x456-<?php echo $n->id; ?>.jpg" alt="<?php echo $n->title; ?>"></a>
                            </div>
                            <div class="top-post-details">
                                <a href="<?php echo base_url() . 'noticia/' . $n->id . '/' . str_replace(" ", "-", $n->title); ?>">
                                    <h4><?php echo $n->title; ?></h4>
                                </a>
                                <a href="<?php echo base_url() . 'noticia/' . $n->id . '/' . str_replace(" ", "-", $n->title); ?>">
                                    <ul class="meta">
                                        <li><span class="lnr lnr-calendar-full"></span><?php
                                            $start_date = $n->create_date;
                                            $date_diff = abs((strtotime("now") - 60 * 60 * 2) - strtotime($start_date));

                                            $years = floor($date_diff / (365 * 60 * 60 * 24));
                                            $months = floor(($date_diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                            $days = floor(($date_diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                                            $hours = floor(($date_diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
                                            $minutes = floor(($date_diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / 60);

                                            if ($years > 0) {
                                                condição ? codigoUm : codigoDois;
                                                $years == 1 ? printf("há %d ano", $years) : printf("há %d anos", $years);;
                                            } else {
                                                if ($months > 0) {
                                                    $months == 1 ? printf("há %d mês", $months) : printf("há %d meses", $months);
                                                } else {
                                                    if ($days > 0) {
                                                        $days == 1 ? printf("há %d dia", $days) : printf("há %d dias", $days);
                                                    } else {
                                                        if ($hours > 0) {
                                                            $hours == 1 ? printf("há %d hora", $hours) : printf("há %d horas", $hours);
                                                        } else {
                                                            if ($minutes > 0) {
                                                                $minutes == 1 ? printf("há %d minuto", $minutes) : printf("há %d minutos", $minutes);
                                                            }
                                                        }
                                                    }

                                                }
                                            } ?></li>
                                        <li><span class="lnr lnr-user"></span><?php echo $n->user_name; ?></li>
                                    </ul>
                                </a>
                            </div>
                            <ul class="category-image-top-right">
                                <li>
                                    <a href="<?php echo base_url() . 'categoria/' . $n->category_id; ?>"><?php echo $n->category_name; ?></a>
                                </li>
                            </ul>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End top-post Area -->
    <!-- Start latest-post Area -->
    <section class="latest-post-area">
        <div class="container no-padding">
            <div class="row">
                <div class="col-lg-8 post-list">
                    <!-- Start latest-post Area -->
                    <div class="latest-post-wrap">
                        <?php foreach (array_slice($one_category, 3) as $n) { ?>
                            <h4 class="cat-title">MAIS DO ESPORTIVO NEWS</h4>
                            <div class="single-latest-post row align-items-center">
                                <div class="col-lg-5 post-left">
                                    <div class="feature-img relative">
                                        <div class="overlay overlay-bg"></div>
                                        <a href="<?php echo base_url() . 'noticia/' . $n->id . '/' . str_replace(" ", "-", $n->title); ?>"><img
                                                    class="img-fluid" srcset="https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/540x300-<?php echo $n->id; ?>.jpg 1x,https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/810x456-<?php echo $n->id; ?>.jpg 1.5x,https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/1080x608-<?php echo $n->id; ?>.jpg 2x"
                                                    src="https://portalnewsimage.s3.sa-east-1.amazonaws.com/<?php echo $n->id; ?>/810x456-<?php echo $n->id; ?>.jpg"
                                                    alt="<?php echo $n->title; ?>"></a>
                                    </div>
                                    <ul class="tags">
                                        <li>
                                            <a href="<?php echo base_url() . 'categoria/' . $n->category_id; ?>"><?php echo $n->category_name; ?></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-7 post-right">
                                    <a href="<?php echo base_url() . 'noticia/' . $n->id . '/' . str_replace(" ", "-", $n->title); ?>">
                                        <h3><?php echo $n->title; ?></h3>
                                    </a>
                                    <a href="<?php echo base_url() . 'noticia/' . $n->id . '/' . str_replace(" ", "-", $n->title); ?>">
                                        <ul class="meta">
                                            <li><span class="lnr lnr-calendar-full"></span><?php
                                                $start_date = $n->create_date;
                                                $date_diff = abs((strtotime("now") - 60 * 60 * 2) - strtotime($start_date));

                                                $years = floor($date_diff / (365 * 60 * 60 * 24));
                                                $months = floor(($date_diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                                $days = floor(($date_diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                                                $hours = floor(($date_diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
                                                $minutes = floor(($date_diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / 60);

                                                if ($years > 0) {
                                                    condição ? codigoUm : codigoDois;
                                                    $years == 1 ? printf("há %d ano", $years) : printf("há %d anos", $years);;
                                                } else {
                                                    if ($months > 0) {
                                                        $months == 1 ? printf("há %d mês", $months) : printf("há %d meses", $months);
                                                    } else {
                                                        if ($days > 0) {
                                                            $days == 1 ? printf("há %d dia", $days) : printf("há %d dias", $days);
                                                        } else {
                                                            if ($hours > 0) {
                                                                $hours == 1 ? printf("há %d hora", $hours) : printf("há %d horas", $hours);
                                                            } else {
                                                                if ($minutes > 0) {
                                                                    $minutes == 1 ? printf("há %d minuto", $minutes) : printf("há %d minutos", $minutes);
                                                                }
                                                            }
                                                        }

                                                    }
                                                } ?></li>
                                            <li><span class="lnr lnr-user"></span><?php echo $n->user_name; ?></li>
                                        </ul>
                                    </a>
                                </div>
                            </div>
                            <hr>
                        <?php } ?>

                    </div>
                </div>

                <div class="col-lg-4 no-padding text-center">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Arranha-céu grande 300 x 600 -->
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:300px;height:600px"
                         data-ad-client="ca-pub-2940248395445787"
                         data-ad-slot="1919707633"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>

            </div>
        </div>
    </section>
    <!-- End latest-post Area -->
</div>

<!-- start footer Area -->
<footer class="footer-area section-gap">
    <div class="container">
        <div class="footer-bottom row align-items-center">
            <p class="footer-text m-0 col-lg-12 col-md-12 text-center">
                Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                All rights reserved
            </p>
        </div>
    </div>
</footer>
<!-- End footer Area -->
<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-2.2.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/vendor/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js" async></script>
<script src="<?php echo base_url(); ?>assets/js/main.js" async></script>
</body>
</html>